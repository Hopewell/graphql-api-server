package com.graphql.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class People implements Serializable {
    private int count;
    private String next;
    private String previous;
    private List<Person> results;
}
