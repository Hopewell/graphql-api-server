package com.graphql.api.service;

import com.graphql.api.model.People;
import com.graphql.api.model.Person;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@GraphQLApi
@Slf4j
public class GraphQlService {
    private final RestTemplate restTemplate;
    @Value("${url.people}")
    private String peopleUrl;
    @Value("${url.person}")
    private String personUrl;

    public GraphQlService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GraphQLQuery(name = "people")
    public People searchAndListPeople(@GraphQLArgument(name = "page", defaultValue = "0") String page, @GraphQLArgument(name = "searchQuery", defaultValue = "") String searchQuery) {
        Map<String, String> params = new HashMap<>();
        params.put("page", page);
        params.put("searchQuery", searchQuery);
        People resultPeople = restTemplate.getForObject(peopleUrl, People.class, params);
        return resultPeople;
    }

    @GraphQLQuery(name = "person")
    public Person findPersonById(@GraphQLArgument(name = "personId") Long personId) {
        Map<String, Long> params = new HashMap<>();
        params.put("personId", personId);
        Person personPerson = restTemplate.getForObject(personUrl, Person.class, params);
        return personPerson;
    }


}
